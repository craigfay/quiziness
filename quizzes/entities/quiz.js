/**
 * Quiz Entity 
 * 
 * * 'Entities' are objects that embody a small set of application
 * * independent business rules, and the data that they operate on.
 * 
 */

/**
 * @param {string} name - the name of the quiz
 * @param {string} author - the quiz's creator
 * @param {string} description - a brief summary of the quizzes purpose
 * @param {boolean} isOpen - whether or not the quiz is open for submissions
 * @param {array} questions - a list of questions that belong to the quiz
 */
class QuizEntity {
    constructor(name, author, description, questions) {
        this.name = typeof name == 'string' ? name : '';
        this.author = typeof author == 'string' ? author : '';
        this.description = typeof description == 'string' ? description : '';
        this.isOpen = typeof isOpen == 'string' ? isOpen : false;
        this.questions = questions instanceof Array ? questions : Array();
    }
}

module.exports = QuizEntity;
