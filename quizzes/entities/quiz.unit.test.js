/**
 * Test quiz entities
 * 
 */

/**
 * Dependencies
 */
const QuizEntity = require('@root/quizzes/entities/quiz');

test('QuizEntity exists', () => {
    expect(QuizEntity).toBeDefined();
})

test('Quiz entities can be created', () => {
    const quiz = new QuizEntity();
    expect(quiz instanceof QuizEntity).toBeTruthy();
    expect(typeof quiz).toBe('object');
})

test('Quiz entities should have a name property', () => {
    expect(new QuizEntity().name).toBeDefined();
})

test('A quiz entity\'s name property should be a string', () => {
    expect(typeof new QuizEntity().name).toBe('string');
    expect(typeof new QuizEntity(7).name).toBe('string');
    expect(typeof new QuizEntity([]).name).toBe('string');
    expect(typeof new QuizEntity({}).name).toBe('string');
    expect(typeof new QuizEntity(true).name).toBe('string');
    expect(typeof new QuizEntity(false).name).toBe('string');
})

test('Quiz entities should have an author property', () => {
    expect(new QuizEntity().author).toBeDefined();
})

test('A quiz entity\'s author property should be a string', () => {
    expect(typeof new QuizEntity().author).toBe('string');
    expect(typeof new QuizEntity('', 4).author).toBe('string');
    expect(typeof new QuizEntity('', []).author).toBe('string');
    expect(typeof new QuizEntity('', {}).author).toBe('string');
    expect(typeof new QuizEntity('', true).author).toBe('string');
    expect(typeof new QuizEntity('', false).author).toBe('string');
})

test('Quiz entities should have an description property', () => {
    expect(new QuizEntity().description).toBeDefined();
})

test('A quiz entity\'s description property should be a string', () => {
    expect(typeof new QuizEntity().description).toBe('string');
    expect(typeof new QuizEntity('', 12).description).toBe('string');
    expect(typeof new QuizEntity('', []).description).toBe('string');
    expect(typeof new QuizEntity('', {}).description).toBe('string');
    expect(typeof new QuizEntity('', true).description).toBe('string');
    expect(typeof new QuizEntity('', false).description).toBe('string');
})

test('Quiz entities should have an isOpen property', () => {
    expect(new QuizEntity().isOpen).toBeDefined();
})

test('A quiz entity\'s isOpen property should be a boolean', () => {
    expect(typeof new QuizEntity().isOpen).toBe('boolean');
    expect(typeof new QuizEntity('', 12).isOpen).toBe('boolean');
    expect(typeof new QuizEntity('', []).isOpen).toBe('boolean');
    expect(typeof new QuizEntity('', {}).isOpen).toBe('boolean');
    expect(typeof new QuizEntity('', '').isOpen).toBe('boolean');
    expect(typeof new QuizEntity('', null).isOpen).toBe('boolean');
})

test('Quiz entities should have a questions property', () => {
    expect(new QuizEntity().questions).toBeDefined();
})

test('A quiz entity\'s questions property should be an array', () => {
    expect(new QuizEntity().questions instanceof Array).toBe(true);
    expect(new QuizEntity('', '', 6).questions instanceof Array).toBe(true);
    expect(new QuizEntity('', '', '').questions instanceof Array).toEqual(true);
})
