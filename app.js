/**
 * Primary server module
 */

/**
 * Dependencies
 */
const config = require('./config');
const database = require('./boundaries/persistence/mongoose/database');
const webserver = require('./boundaries/web/server.js');

/**
 * Handle Errors
 */
process.on('unhandledRejection', error => {
    console.log('unhandledRejection', error);
})

process.on('uncaughtException', error => {
    console.log('uncaughtException', error);
})

/**
 * Open External boundaries
 */
const persistence = database.init(config);
const web = webserver.init(config);
