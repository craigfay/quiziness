/**
 * Test the integration of the Mongoose persistence boundary implementation
 * with the rest of the system
 * 
 */

/**
 * Dependencies
 */
const database = require('@root/boundaries/persistence/mongoose/database')
const Account = require('@root/accounts/entity')
const config = require('@root/config')

beforeAll(async () => {
    await database.init(config);
})
        
afterAll(async () => {})
            
// @todo Run mongod automatically
test('New Accounts can be persisted', async () => {
    const account = new Account(
        'Charles',
        'Xavier',
        'charles@xavier.com',
        'password'
    )
    const operation = await database.insertAccount(account);
    expect(operation.successful).toBe(true);
})

test('Accounts can be retrieved individually by email address', async () => {
    const operation = await database.fetchAccount('charles@xavier.com');
    expect(operation.successful).toBe(true);
})

test('Persisted Accounts should have all the same properties as an Account Entity',  async() => {
    const operation = await database.fetchAccount('charles@xavier.com');
    for (property of Object.keys(new Account())) {
        expect(operation.data).toHaveProperty(property);
    }
})

test('Existing Accounts can have their verification status set as true', async() => {
    const operation = await database.confirmAccountVerification('charles@xavier.com');
    expect(operation.successful).toBe(true);
})

test('Existing Accounts can be deleted', async () => {
    const email = 'charles@xavier.com';
    const operation = await database.deleteAccount(email);
    expect(operation.successful).toBe(true);
})
