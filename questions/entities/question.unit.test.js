/**
 * Test question entities
 * 
 */

/**
 * Dependencies
 */
const QuestionEntity = require('@root/questions/entities/question');

test('QuestionEntity exists', () => {
    expect(QuestionEntity).toBeDefined();
})

test('Question entities can be created', () => {
    const question = new QuestionEntity();
    expect(question instanceof QuestionEntity).toBeTruthy();
    expect(typeof question).toBe('object');
})

test('Question entities should have a body property', () => {
    expect(new QuestionEntity().body).toBeDefined();
})

test('A question entity\'s body property should be a string', () => {
    expect(typeof new QuestionEntity().body).toBe('string');
    expect(typeof new QuestionEntity(7).body).toBe('string');
    expect(typeof new QuestionEntity([]).body).toBe('string');
    expect(typeof new QuestionEntity({}).body).toBe('string');
    expect(typeof new QuestionEntity(true).body).toBe('string');
    expect(typeof new QuestionEntity(false).body).toBe('string');
})

test('Question entities should have an acceptedAnswers property', () => {
    expect(new QuestionEntity().acceptedAnswers).toBeDefined();
})

test('A question entity\'s acceptedAnswers property should be an array', () => {
    expect(new QuestionEntity().acceptedAnswers instanceof Array).toBe(true);
})

test('Question entities should have an accepts method', () => {
    expect(typeof new QuestionEntity().accepts).toBe('function');
})

test('A question entity\'s accept method should return a boolean', () => {
    expect(typeof new QuestionEntity().accepts('')).toBe('boolean');
})

test('A question entity\'s accept method should indicate whether an answer is in acceptedAnswers', () => {
    expect(new QuestionEntity('', ['one', 'two', 'three']).accepts('')).toBe(false);
    expect(new QuestionEntity('', ['one', 'two', 'three']).accepts('one')).toBe(true);
})

test('A question entity\'s accept method should be case insensitive', () => {
    expect(new QuestionEntity('', ['one', 'two', 'three']).accepts('ONE')).toBe(true);
})