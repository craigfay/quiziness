/**
 * Question Entity 
 * 
 * * 'Entities' are objects that embody a small set of application
 * * independent business rules, and the data that they operate on.
 * 
 */

/**
 * @param {string} body - The textual representation of the problem posed by the question
 * @param {array} acceptedAnswers - A list of answers considered 'correct'
 * 
 */
class QuestionEntity {
    constructor(body, acceptedAnswers) {
        this.body = typeof body == 'string' ? body : '';
        this.acceptedAnswers = acceptedAnswers instanceof Array ? acceptedAnswers : Array();
    }
    accepts(answer) {
        return this.acceptedAnswers.map(a => a.toLowerCase()).includes(answer.toLowerCase());
    }
}

module.exports = QuestionEntity;