/**
 * Test question entities
 * 
 */

/**
 * Dependencies
 */
const SubmissionEntity = require('@root/submissions/entities/submission');

test('SubmissionEntity exists', () => {
    expect(SubmissionEntity).toBeDefined();
})

test('Submission entities can be created', () => {
    const submission = new SubmissionEntity();
    expect(submission instanceof SubmissionEntity).toBeTruthy();
    expect(typeof submission).toBe('object');
})

test('Submission entities should have a submitter property', () => {
    expect(new SubmissionEntity().submitter).toBeDefined();
})

test('A submission entity\'s submitter property should be a string', () => {
    expect(typeof new SubmissionEntity().submitter).toBe('string');
})

test('Submission entities should have a quiz property', () => {
    expect(new SubmissionEntity().quiz).toBeDefined();
})

test('A submission entity\'s quiz property should be a string', () => {
    expect(typeof new SubmissionEntity().quiz).toBe('string');
})

test('Submission entities should have an answers property', () => {
    expect(new SubmissionEntity().answers).toBeDefined();
})

test('A submission entity\'s answers property should be an object', () => {
    expect(typeof new SubmissionEntity().answers).toBe('object');
})

test('Submission entities should have an graded property', () => {
    expect(new SubmissionEntity().graded).toBeDefined();
})

test('A submission entity\'s answers property should be an boolean', () => {
    expect(typeof new SubmissionEntity().graded).toBe('boolean');
})

test('Submission entities should have an score property', () => {
    expect(new SubmissionEntity().score).toBeDefined();
})

test('Submission entities should be defaulted to graded: false, and score: 0.0.', () => {
    let submission = new SubmissionEntity();
    expect(submission.graded).toBe(false);
    expect(submission.score).toBe(0.0);
})

test('Submission scores can be set to values between 0 and 1 with applyScore()', () => {
    let submission = new SubmissionEntity();
    submission.applyScore(.8);
    expect(submission.score).toBe(.8);
    submission.applyScore(1);
    expect(submission.score).toBe(1.0);
    submission.applyScore(-1);
    expect(submission.score).toBe(1.0);
    submission.applyScore(2);
    expect(submission.score).toBe(1.0);
})

test('SubmissionEntity.applyScore() will return true on successful update, false on failure', () => {
    let submission = new SubmissionEntity();    
    expect(submission.applyScore(-5)).toBe(false);    
    expect(submission.applyScore('hello')).toBe(false);    
    expect(submission.applyScore(.25)).toBe(true);    
    expect(submission.applyScore(0)).toBe(true);    
})