/**
 * Submission Entity 
 * 
 * * 'Entities' are objects that embody a small set of application
 * * independent business rules, and the data that they operate on.
 * 
 */

/**
 * @param {string} submitter - An identifier of the submitter (not necessarily unique)
 * @param {string} quiz - An unique identifier of the quiz that the submission corresponds to 
 * @param {object} answers - A container of questions identifiers corresponding to answer objects  
 */
class SubmissionEntity {
    constructor(submitter, quiz, answers) {
        this.submitter = typeof submitter == 'string' ? submitter : '';
        this.quiz = typeof quiz == 'string' ? quiz : '';
        this.answers = typeof answers == 'object' ? answers : Object();
        this.graded = false;
        this.score = 0.0;
    }
    applyScore(score) {
        if (score >= 0 && score <= 1) {
            this.score = score;
            this.graded = true;
            return true;
        } 
        return false;
    }
}

module.exports = SubmissionEntity;
