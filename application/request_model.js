/**
 * RequestModel Class
 * Instances of this class contain data about a account interaction with the system.
 * 
 */

const RequestModel = class {
    /**
     * @param {Object} body - Details of a account request 
     */
    constructor(body) {
        // Accept all Objects, except Arrays
        this.body = body instanceof Object && ! (body instanceof Array) ? body : {};
    }
    has(property) {
        try {
            if (this.body[property]) return true;
        }
        catch(e) {
            return false;
        }
    }
}

module.exports = RequestModel;
