/**
 * Dependencies
 */
const ErrorModel = require('@application/error_model');

test('true should be true', () => {
    expect(true).toBe(true);
})

test('ErrorModels should have a type property', () => {
    expect(new ErrorModel().type).toBeDefined();
})

test('ErrorModels should have a message property', () => {
    expect(new ErrorModel().message).toBeDefined();
})

test('ErrorModels should default to an Implementation Error if not instantiated properly', () => {
    expect(new ErrorModel().type).toBe('Implementation Error');
})

test('ErrorModels should indicate when they\'re not instantiated properly with their message property', () => {
    expect(new ErrorModel().message).toBe('ErrorModel was not constructed correctly');
})

test('ErrorModels should default to an Implementation Error if not passed two strings as arguments', () => {
    expect(new ErrorModel(5).type).toBe('Implementation Error')
})