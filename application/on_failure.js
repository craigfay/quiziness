/**
 * This module provides access to a user-friendly 
 * ResponseModel when the application fails over boundaries.
 */

/**
 * Dependencies
 */
const ResponseModel = require('./response_model');
const ErrorModel = require('./error_model');

const onFailure = () => {
    return new ResponseModel(
        false,
        new ErrorModel(
            'Server Error',
            'The Server could not handle the request at this time. Please Try again later!'
        )
    )
}

module.exports = onFailure;