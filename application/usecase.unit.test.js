/**
 * Dependencies
 */
const UseCase = require('./usecase');

test('UseCase should exist', () => {
    expect(UseCase).toBeDefined();
})

test('UseCase should have an onFailure method', () => {
    expect(UseCase).toHaveProperty('onFailure');
})