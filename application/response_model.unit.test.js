/**
 * Dependencies
 */
const ResponseModel = require('@application/response_model');

test('ResponseModel should be capable of instantiation', () => {
    expect(new ResponseModel() instanceof ResponseModel).toBe(true);
})

test('ResponseModel instances should have a successful property', () => {
    expect(new ResponseModel().successful).toBeDefined();
})

test('ResponseModel instances should have an error property', () => {
    expect(new ResponseModel().error).toBeDefined();
})

test('ResponseModel instances should have a data property', () => {
    expect(new ResponseModel().data).toBeDefined();
})