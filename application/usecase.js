/**
 * Dependencies
 */
const ResponseModel = require('./response_model');
const ErrorModel = require('./error_model');

/**
 * Use Case class
 */
const UseCase = class {
    /**
     * The method is a macroscopic error catcher for Use Cases 
     * @param {Error} error 
     * @todo Create an error logging system
     * @todo Create uniformity in ErrorModel types and messages
     */
    static onFailure(error) {
        console.log('Usecase Failure:', UseCase.shortenError(error));
        return new ResponseModel(
            false,
            new ErrorModel(
                'Server Error',
                'The server has experienced an unexpected error'
            )
        );
    }

    /**
     * Create a shortened but descriptive summary of an error
     * @todo this should probably live elsewhere
     * @param {Error} error 
     */
    static shortenError(error) {
        return [error.name, error.message, error.stack.match(/\(.*\)/)[0], '\n'].join(' ');
    }
}

module.exports = UseCase;