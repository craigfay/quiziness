/**
 * ReponseModel Class
 * Instances of this class contain data about the results of operations requested by accounts.
 * 
 */

/**
 * Dependencies
 */
const ErrorModel = require('./error_model');

const ResponseModel = class {
    /**
     * @param {Boolean} successful - Whether or not an attempted operation was successful 
     * @param {ErrorModel | null} error - (Nullable) An ErrorModel that describes why an operation was unsuccessful 
     * @param {Object} data - Data to be returned to the requester 
     * @todo Should error be assumed not to exist?
     */
    constructor(successful=false, error=false, data=false) {

        // Error Properties must be null, or instances of the ErrorModel class
        if (error instanceof ErrorModel || error == null) {
            // Proceed normally
            this.successful = typeof successful == 'boolean' ? successful : false;
            this.error = error;
        } else {
            // Force successful to be false and create a new ErrorModel
            this.successful = false;
            this.error = new ErrorModel('Implementation Error', 'ResponseModel did not receive a proper Error');
        }
        // Data property
        this.data = typeof data == 'object' ? data : {};
    }
}

module.exports = ResponseModel;
