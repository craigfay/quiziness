/**
 * ErrorModel Class
 * Instances of this class contain details about the failure of a request to the system
 * 
 */

const ErrorModel = class {
    /**
     * @param {String} type - A general and standardized category of the error
     * @param {String} message - The account-readable description of the error 
     */
    constructor (type=false, message=false) {
        // If the class was not constructed properly, default to an Implementation Error
        if (typeof type != 'string' || typeof message != 'string') {
            this.type = 'Implementation Error';
            this.message = 'ErrorModel was not constructed correctly';
        }
        else {
            this.type = type;
            this.message = message;
        }
    }
}

module.exports = ErrorModel;