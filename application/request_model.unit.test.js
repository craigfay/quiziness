/**
 * Dependencies
 */
const RequestModel = require('@application/request_model');

test('RequestModel should be capable of instantiation', () => {
    expect(new RequestModel() instanceof RequestModel).toBe(true);
})

test('RequestModel instances should have a body property', () => {
    expect(new RequestModel().body).toBeDefined();
})

test('RequestModel should default the body property to an object', () => {
    expect(new RequestModel(5).body).toEqual({});
    expect(new RequestModel('hello').body).toEqual({});
    expect(new RequestModel([4, 2, 6, 7]).body).toEqual({});
})

test('RequestModel should use unaltered body parameters if they are objects', () => {
    const args = {name: 'Dylan', email: 'dylan@dinosaurs.com'};
    expect(new RequestModel(args).body).toEqual(args);
})

