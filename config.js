/**
 * Configuration Variables
 * 
 */

// Export Wrappers
const config = {};
const testconfig = {};

// Persistence Implementation
config.persistence = require('./boundaries/persistence/mongoose/database');
testconfig.persistence = require('./boundaries/persistence/testdummy');

/*
 * Database variables
 */
config.db = {
    prefix: 'mongodb',
    address: 'localhost',
    port: '27017',
    name: 'real_db',
};
config.db.url = `${config.db.prefix}://${config.db.address}:${config.db.port}/${config.db.name}`;

testconfig.db = {
    prefix: 'mongodb',
    address: 'localhost',
    port: '27017',
    name: 'test_db',
};
testconfig.db.url = `${testconfig.db.prefix}://${testconfig.db.address}:${testconfig.db.port}/${testconfig.db.name}`;



// Used to hash account passwords
config.secretkey = 'How is a raven like a writing desk?';
testconfig.secretkey = 'How is a raven like a writing desk?';

// If the PORT environment variable is set, use it for the port. Default to 3000.
config.port = process.env.PORT || 4000;
testconfig.port = process.env.PORT || 4000;


module.exports = process.env.NODE_ENV != 'test' ? config : testconfig;
