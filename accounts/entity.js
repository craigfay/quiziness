/**
 * Account Entity 
 * 
 * * 'Entities' are objects that embody a small set of application
 * * independent business rules, and the data that they operate on.
 * 
 */

/**
 * Dependencies
 */
const bcrypt = require('bcrypt');

/**
 * @param {string} firstname - the account's first name
 * @param {string} lastname - the account's last name
 * @param {object} email - a unique email address associated with the account
 * @param {string} password - a secret value that can be used to verify a account's identity
 * @todo What is the role of static methods?
 */
class AccountEntity {
    constructor(firstname, lastname, email, password) {
        this.firstname = typeof firstname == 'string' ? this.nameCase(firstname) : '',
        this.lastname = typeof lastname == 'string' ? this.nameCase(lastname) : '',
        this.email = typeof email == 'string' ? email.toLowerCase() : '',
        this.password = typeof password == 'string' ? this.hash(password) : '',
        this.verified = false;
    }
    nameCase(str) {
        return str.charAt(0).toUpperCase() + str.slice(1).toLowerCase();
    }

    /**
     * Each of the validation methods follow this pattern:
     * @param {String} value
     * @returns {Boolean} Whether or not the value meets our requirements
     */

    static validateFirstname(firstname) {
        return typeof firstname == 'string' && /^[A-Z]{2,16}$/i.test(firstname);
    }
    static validateLastname(lastname) {
        return typeof lastname == 'string' && /^[A-Z]{2,16}$/i.test(lastname);
    }
    static validateEmail(email) {
        return typeof email == 'string' && /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(email);
    }
    static validatePassword(password) {
        return typeof password == 'string' && /^[A-Z0-9!@#$%^&*]{8,32}$/i.test(password);
    }

    static firstnameRequirements() {
        return 'First Name must be between 2 and 16 characters long, and use only alphabetical characters';
    }
    static lastnameRequirements() {
        return 'Last Name must be between 2 and 16 characters long, and use only alphabetical characters';
    }
    static emailRequirements() {
        return 'Email must be a valid email address';
    }
    static passwordRequirements() {
        return 'Password must be between 8 and 16 characters long, and use only alphanumeric/special characters';
    }

    /**
     * @param {String} password
     * @returns {String}
     * @todo Does this 2nd parameter need to change?
     */
    hash(password) {
        return bcrypt.hashSync(password, 5);
    }

    /**
     * @param {String} plaintext
     * @param {String} hash
     * @returns {Boolean}
     */
    static compareHash(plaintext, hash) {
        return bcrypt.compareSync(plaintext, hash);
    } 
}

module.exports = AccountEntity;
