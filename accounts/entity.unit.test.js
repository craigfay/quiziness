/**
 * Test account entities 
 * 
 */

/**
 * Dependencies
 */
const Account = require('@root/accounts/entity');

test('Account exists', () => {
    expect(Account).toBeDefined();
})

test('Account entities can be created', () => {
    const account = new Account();
    expect(account instanceof Account).toBeTruthy();
    expect(typeof account).toBe('object');
})

test('Non-string firstnames default to empty strings', () => {
    expect(new Account(5).firstname).toBe('');
    expect(new Account([]).firstname).toBe('');
    expect(new Account({}).firstname).toBe('');
    expect(new Account(false).firstname).toBe('');
    expect(new Account(true).firstname).toBe('');
})

test('Non-string lastnames default to empty strings', () => {
    expect(new Account(5).lastname).toBe('');
    expect(new Account([]).lastname).toBe('');
    expect(new Account({}).lastname).toBe('');
    expect(new Account(false).lastname).toBe('');
    expect(new Account(true).lastname).toBe('');
})

test('Non-string passwords default to empty strings', () => {
    expect(new Account('', 5).password).toBe('');
    expect(new Account('', []).password).toBe('');
    expect(new Account('', {}).password).toBe('');
    expect(new Account('', false).password).toBe('');
    expect(new Account('', true).password).toBe('');
})

test('String firstnames will be stored with name-casing', () => {
    expect(new Account('SAM').firstname).toBe('Sam');
    expect(new Account('amY').firstname).toBe('Amy');
})

test('String lastnames will be stored with name-casing', () => {
    expect(new Account('sophie', 'williamson').lastname).toBe('Williamson');
    expect(new Account('Carlo', 'sEbastIan').lastname).toBe('Sebastian');
})

test('String passwords will be hashed before storage', () => {
    const password = '4hf9ah8q';
    const account = new Account('Sam', 'Capps', 'sam@email.com', password);
    expect(account.password == password).toBe(false);
    expect(Account.compareHash(password, account.password)).toBe(true);
})

test('Accounts will have a property called "verified"', () => {
    const account = new Account('Sam', 'Capps', 'sam@email.com', 'passowrd'); 
    expect(account).toHaveProperty('verified');
})

test('Accounts\'s "verified" property will always initialized as false', () => {
    const account = new Account('Sam', 'Capps', 'sam@email.com', 'passowrd'); 
    expect(account.verified).toBe(false);
})

