/**
 * Dependencies
 */
const UseCase = require('../../application/usecase');
const ErrorModel = require('../../application/error_model');
const RequestModel = require('../../application/request_model');
const ResponseModel = require('../../application/response_model');
const Account = require('../entity');
const { persistence } = require('../../config');

module.exports = class AccountLogin extends UseCase {

    static async execute(request) {
        try {
            // Reject Requests that aren't RequestModel objects
            if (request instanceof RequestModel == false) {
                return new ResponseModel(
                    false,
                    new ErrorModel(
                        'Implementation Error',
                        'The UseCase request was not formatted correctly'
                    )
                );
            }
            const retrieval = await AccountLogin.retrieveData(request);
            return AccountLogin.createResponse(request, retrieval);
        }
        catch(e) {
            return AccountLogin.onFailure(e);
        }
    }

    static async retrieveData(request) {
        // Check for an account with the given email and password
        const { email, password } = request.body;
        return await persistence.fetchAccount(email, password);
    }

    static createResponse(request, retrieval) {
        // Catch Accounts that exist, but are unverified
        if (retrieval.successful && retrieval.data.verified == false) {
            return new ResponseModel(
                false,
                new ErrorModel(
                    'Invalid Request',
                    'So close! Please verify your email address before continuing.'
                )
            )
        }

        // Success
        if (retrieval.successful && Account.compareHash(request.body.password, retrieval.data.password)) {
            // The account exists AND the password is correct 
            return new ResponseModel(
                true,
                null,
                {}
            )
        }

        // All failures related to persistence
        if (retrieval.successful == false) {
            if (retrieval.error.type == 'Not Found') {
                // There were no accounts matching the email provided'
                return new ResponseModel(
                    false,
                    new ErrorModel(
                        'Invalid Request',
                        'The username and password combination does not exist.'
                    )
                );
            }
            // Any other reason persistence may have been unsuccessful 
            else {
                return new ResponseModel(
                    false,
                    new ErrorModel(
                        'Server Error',
                        'The server could not properly handle the Login Request.'
                    )
                );
            }
        }

        // The account exists, but the password is incorrect
        return new ResponseModel(
            false,
            new ErrorModel(
                'Invalid Request',
                'The username and password combination does not exist.'
            )
        );  
    }
}
