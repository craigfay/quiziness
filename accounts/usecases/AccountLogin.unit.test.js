/**
 * Dependencies
 */
const UseCase = require('@application/usecase')
const ResponseModel = require('@application/response_model');
const AccountLogin = require('./AccountLogin');

test('AccountLogin should have an execute property', () => {
    expect(AccountLogin).toHaveProperty('execute');
})

test('AccountLogin.execute() should resolve to a ResponseModel', async () => {
    expect(await AccountLogin.execute() instanceof ResponseModel).toBe(true);
})

test('AccountLogin.execute() should always respond negatively when not invoked with a RequestModel', async () => {
    const response = await AccountLogin.execute();
    expect(response.error.message).toBe('The UseCase request was not formatted correctly');
})
