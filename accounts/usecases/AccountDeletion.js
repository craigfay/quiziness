/**
 * Dependencies
 */
const UseCase = require('../../application/usecase');
const ErrorModel = require('../../application/error_model');
const RequestModel = require('../../application/request_model');
const ResponseModel = require('../../application/response_model');
const { persistence } = require('../../config');

module.exports = class AccountDeletion extends UseCase {
    
    static async execute(request) {
        
        // This is the highest level of error handling for this use case 
        try {
            // Reject Requests that aren't RequestModel objects
            if (request instanceof RequestModel == false) {
                return new ResponseModel(
                    false,
                    new ErrorModel(
                        'Implementation Error',
                        'The UseCase request was not formatted correctly'
                    )
                );
            }

            const operation = await persistence.deleteAccount(request.email);

            // Success / Failure
            // @todo Determine where each kind of error is identified
            return operation;
        }

        catch (e) {
            return AccountDeletion.onFailure(e);
        }
    }

}