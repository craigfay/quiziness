/**
 * Dependencies
 */
const UseCase = require('@application/usecase')
const ResponseModel = require('@application/response_model');
const AccountRegistration = require('./AccountRegistration');

test('AccountRegistration should have an execute property', () => {
    expect(AccountRegistration).toHaveProperty('execute');
})

test('AccountRegistration.execute() should resolve to a ResponseModel', async () => {
    expect(await AccountRegistration.execute() instanceof ResponseModel).toBe(true);
})

test('AccountRegistration.execute() should always respond negatively when not invoked with a RequestModel', async () => {
    result = await AccountRegistration.execute();
    expect(result.error.message).toBe('The UseCase request was not formatted correctly');
})
