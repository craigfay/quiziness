/**
 * Dependencies
 */
const UseCase = require('@application/usecase')
const RequestModel = require('@application/request_model');
const ResponseModel = require('@application/response_model');
const AccountDeletion = require('./AccountDeletion');

test('AccountDeletion should have an execute property', () => {
    expect(AccountDeletion).toHaveProperty('execute');
})

test('AccountDeletion.execute() should resolve to a ResponseModel', async () => {
    expect(await AccountDeletion.execute() instanceof ResponseModel).toBe(true);
})

test('AccountDeletion.execute() should always respond negatively when not invoked with RequestModel', async () => {
    const response = await AccountDeletion.execute();
    expect(response.error.message).toBe('The UseCase request was not formatted correctly');
})

test('Existing Accounts can be deleted', async () => {
    const request = new RequestModel({email: 'charles@xavier.com'})
    const response = await AccountDeletion.execute(request);
})