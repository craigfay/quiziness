/**
 * Dependencies
 */
const UseCase = require('../../application/usecase');
const ErrorModel = require('../../application/error_model');
const RequestModel = require('../../application/request_model');
const ResponseModel = require('../../application/response_model');
const Account = require('../entity');
const { persistence } = require('../../config');

module.exports = class AccountRegistration extends UseCase {

    static async execute(request) {

        // Validate request
        const validation = AccountRegistration.validate(request);
        if (false == validation.successful) {
            return validation;
        }

        // Instantiate a new account entity
        const account = new Account(
            request.body.firstname,
            request.body.lastname,
            request.body.email,
            request.body.password
        );

        // Attempt to persist the Account Entity
        const operation = await persistence.insertAccount(account)
        return operation;
    }

    static validate(request) {
        // Reject Requests that aren't RequestModel objects
        if (request instanceof RequestModel == false) {
            return new ResponseModel(
                false,
                new ErrorModel(
                    'Implementation Error',
                    'The UseCase request was not formatted correctly'
                )
            );
        }
        // Validate each of the required properties to register an account
        if  (false == Account.validateFirstname(request.body.firstname)) {
            return new ResponseModel( false, new ErrorModel('Invalid Request', Account.firstnameRequirements()));
        }
        if  (false == Account.validateLastname(request.body.lastname)) {
            return new ResponseModel( false, new ErrorModel('Invalid Request', Account.lastnameRequirements()));
        }
        if  (false == Account.validateEmail(request.body.email)) {
            return new ResponseModel( false, new ErrorModel('Invalid Request', Account.emailRequirements()));
        }
        if  (false == Account.validatePassword(request.body.password)) {
            return new ResponseModel( false, new ErrorModel('Invalid Request', Account.passwordRequirements()));
        }

        // Valid
        return new ResponseModel(true, null);
    }
}
