/**
 * A Boundary Object that opens communication with a database
 * 
 * Terminal Usage:  mongod --dbpath data/db
 * ^This command must be run before MongoDB can be connected 
 * 
 */

/**
 * Dependencies
 */
const mongoose = require('mongoose');
const Account = require('./models/account');
const PersistenceBoundary = require('../interface');
const ErrorModel = require('../../../application/error_model')
const ResponseModel = require('../../../application/response_model')

/**
 * Export Wrapper
 */
const implementation = {};

/** @function implementation.init
 * Open a connection with a MongoDB Database 
 * @param {Object} config - A configuration object with a port property 
 */
implementation.init = async (config) => {
    /**
     * Connect to the database
     */
    try {
        const connection = await mongoose.connect(config.db.url, { useNewUrlParser: true });
    }
    catch(e) {
        return false;
    }
    if (process.env.NODE_ENV !== "test") {
        console.log(`Database connected at ${config.db.url} ...`);
    }
    return true;

}

implementation.close = async () => {
    try {
        await mongoose.connection.close()
        if (process.env.NODE_ENV !== "test") {
            console.log('Database disconnected ... ');
        }
        return true;
    }
    catch(e) {
        return false;
    }
}

/**
 * @param {AccountEntity} entity
 * @returns {RespnseModel}
 */
implementation.insertAccount = async (entity) => {
        const account = new Account({
            'firstname': entity.firstname,
            'lastname': entity.lastname,
            'password': entity.password,
            'email': entity.email,
            'verified': entity.verified
        })

        // Can we do this with await?
        return account.save()
            .then(result => {
                return {
                    successful: true,
                    error: null,
                    data: entity  
                }
            })
            .catch(error => {
                // Duplicate Key Error
                if (error.code == 11000) {
                    return new ResponseModel(
                        false,
                        new ErrorModel(
                            'Persistence Error',
                            'That email is already associated with an account. Please choose another.'
                        ),
                        null
                    )
                }
                // Any other persistence error
                return new ResponseModel(
                    false,
                    new ErrorModel(
                        'Persistence Error',
                        'There was a problem storing the new account data. Please try again.'
                    ),
                    null
                )
            })
}

/**
 * Fetch a single account given an email address
 */
implementation.fetchAccount = async (email) => {
    const results = await Account.find({email})
    
    // Success
    if (results.length == 1) {
        return new ResponseModel(
            true,
            null,
            results[0]
        )
    }
    
    // Failure
    if (results.length == 0) {
        return new ResponseModel(
            false,
            new ErrorModel(
                'Not Found',
                'There were no accounts matching the email provided'
            )
        )
    }
    if (results.length > 1) {
        return new ResponseModel(
            false,
            new ErrorModel(
                'Persistence Error',
                'There were multiple accounts matching the specified email'
            ),
            results
        )
    }
    return new ResponseModel(
        false,
        new ErrorModel(
            'Persistence Error',
            'There was a problem checking for an account'
        ),
        null
    )
}

/**
 * Delete a single account record
 * @param {String} email
 * @todo wrap with try/catch?
 */
implementation.deleteAccount = async (email) => {
    const results = await Account.deleteOne({email})

    // Success
    if (results.ok == 1) {
        return new ResponseModel(
            true,
            null,
            {email}
        )
    }
    // Failure
    return new ResponseModel(
        false,
        new ErrorModel(
            'Persistence Error',
            'There was a problem deleting an account'
        ),
        null
    )
}

/**
 * Set an account's verification property to true
 * @param {String} email
 */
implementation.confirmAccountVerification = async (email) => {
    const results = await Account.updateOne({'email': email}, {'verified': true})

    // Success
    if (results.ok && results.nModified == 1) {
        return new ResponseModel(
            true,
            null,
            {email}
        )
    }
    // Failure
    return new ResponseModel(
        false,
        new ErrorModel(
            'Persistence Error',
            'There was a problem confirming account verification'
        ),
        null
    )
}

// @todo Automatically remove accounts that have been unverified for 48 hours

module.exports = new PersistenceBoundary(implementation);