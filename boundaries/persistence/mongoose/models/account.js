/**
 * Model for mongo account documents via mongoose
 * 
 */

/**
 * Dependencies
 */
const mongoose = require('mongoose');
const AccountEntity = require('../../../../accounts/entity');
// How can this be made to depend on the domain?

/**
 * Export
 */
const AccountSchema = new mongoose.Schema({
    firstname: {type: String, required: true },
    lastname: {type: String, required: true },
    password: {type: String, required: true },
    email: {type: String, unique: true, required: true },
    verified: {type: Boolean, required: true, default: false}
})

const skipInit = process.env.NODE_ENV === "test";
module.exports = mongoose.model('accounts', AccountSchema, 'accounts', skipInit)
