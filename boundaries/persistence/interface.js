/**
 * An 'interface' to be 'implemented' by persistence solutions.
 * No data should be persisted except by instances of this class.
 * Instances of this class should only be used inside of usecases.
 */

class PersistenceBoundary {
    constructor(implementation) {

        /**
         * A placeholder function that's called when an implementation doesn't have the required methods
         * @param {string} operation - the name of the function that isn't properly implemented
         */
        this.fallback = (operation) => {
            return {
                successful: false,
                error: {
                    type: 'Implementation Error',
                    message: `The persistence operation ${operation} is not implemented correctly.`, 
                },
                data: {}
            }
        }

        /**
         * Create a connection with the persistent destination
         * @param {object} config - Configuration options that the method might need
         */
        this.init = typeof implementation.init == 'function' ? implementation.init : this.fallback('init'); 

        /**
         * Close an existing connection with the persistent destination
         */
        this.close = typeof implementation.close == 'function' ? implementation.close : this.fallback('close'); 

        /**
         * Persist a new Account Entity
         * @param {object} account - the account to insert
         */
        this.insertAccount = typeof implementation.insertAccount == 'function' ? implementation.insertAccount : this.fallback('insertAccount');
        
        /**
         * Check for the existence of an account given an email and password
         * @param {AccountEntity} account - the account to check for 
         */
        this.fetchAccount = typeof implementation.fetchAccount == 'function' ? implementation.fetchAccount : this.fallback('fetchAccount');

        /**
         * Delete an account given an email
         * @param {AccountEntity} account - the account to check for 
         */
        this.deleteAccount = typeof implementation.deleteAccount == 'function' ? implementation.deleteAccount : this.fallback('deleteAccount');

        /**
         * Change an account's verification status to true
         * @param {AccountEntity} account - the account to check for 
         */
        this.confirmAccountVerification = typeof implementation.confirmAccountVerification == 'function' ? implementation.confirmAccountVerification : this.fallback('confirmAccountVerification');
    }
}
module.exports = PersistenceBoundary;