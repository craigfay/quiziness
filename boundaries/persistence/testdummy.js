/**
 * An dummy implementation of PersistenceBoundary defined in boundaries/persistence/interface
 * This implementation is used only for testing
 * 
 */

/**
 * Dependencies
 */
const PersistenceInterface = require('./interface');

// Implementation wrapper
const implementation = {};

implementation.init = (config) => {
    console.log('Connected to a pretend database ...');
    return {
        successful: true,
        error: null,
        data: {}
    }
}

implementation.createAccount = async (account) => {
    return {
            successful: true,
            error: null,
            data: data
    }
}

implementation.fetchAccount = async (email) => {
    return {
            successful: true,
            error: null,
            data: {email}
    }
}

implementation.deleteAccount = async (email) => {
    return {
            successful: true,
            error: null,
            data: {email}
    }
}

module.exports = new PersistenceInterface(implementation);