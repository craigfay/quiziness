/**
 * A module that makes requests to the api
 */

const ip = require('ip');

export default class api {

    // @todo why can't we access this via login? static?
    address() {
        return `${ip.address()}:4000`  
    } 

    static async login(request) {
        let loginAttempt = await fetch(`http://${ip.address()}:4000/api/accounts/login`, {
            method: 'POST',
            headers: {
                'Accept': '*/*',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(request)
        })

        return await loginAttempt.json();
    }

    static async register(request) {
        let loginAttempt = await fetch(`http://${ip.address()}:4000/api/accounts/register`, {
            method: 'POST',
            headers: {
                'Accept': '*/*',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(request)
        })

        return await loginAttempt.json();
    }

    static async protected(token) {
            // Make a request to the api
            let response = await fetch(`http://${ip.address()}:4000/api/accounts/protected`, {
                method: 'POST',
                headers: {
                    'Accept': '*/*',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({token})
            })

            return await response.json();
    }
}