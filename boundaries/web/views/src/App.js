import React from 'react';
import { Route, BrowserRouter, Switch } from 'react-router-dom';
import { TransitionGroup, CSSTransition } from "react-transition-group";

import Dashboard from './components/Dashboard';
import AccountRegistration from './components/AccountRegistration';
import AccountLogin from './components/AccountLogin';
import Protected from './components/Protected';
import containers from './styles/containers.module.css';

/**
 * @todo Share this data with the entire application 
 */
const RegistrationFields = { 
  firstname: {id:'firstname', name: 'First Name', pattern: /^[A-Z]{2,16}$/i, requirements: 'First Name must be between 2 and 16 characters long, and use only alphabetical characters'},
  lastname: {id:'lastname', name: 'Last Name', pattern: /^[A-Z]{2,16}$/i, requirements: 'Last Name must be between 2 and 16 characters long, and use only alphabetical characters'},
  email: {id:'email', name: 'Email', pattern: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i, requirements: 'Email must be a valid email address'},
  password: {id:'password', name: 'Password', pattern: /^[A-Z0-9!@#$%^&*]{8,32}$/i, requirements: 'Password must be between 8 and 16 characters long, and use only alphanumeric/special characters', password: true}
}

const LoginFields = [
  {id:'email', name: 'Email', pattern: /.*/i, requirements: 'Email must be a valid email address'},
  {id:'password', name: 'Password', pattern: /.*/i, requirements: 'Password must be between 8 and 16 characters long, and use only alphanumeric/special characters', password: true}
]



class App extends React.Component {
  render() {
    return (
      <div className={containers.fullheight}>
        <BrowserRouter>
            <Switch>
              <Route path='/dashboard/:subroute' component={Dashboard} exact/>
              <Route path='/register' render={() => <AccountRegistration fields={RegistrationFields}/>}/>
              <Route path='/login' render={() => <AccountLogin fields={LoginFields}/>}/>
            </Switch>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
