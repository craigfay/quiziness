import React from 'react';
import containers from '../styles/containers.module.css';

export default class Example2 extends React.Component {
    constructor() {
        super();
    }

    render() {
        return (
            <div className={containers.scrollable}>
                <div className={containers.rowed}>
                    <div className={containers.columned}>
                        <div className={containers.fullwidth}>
                            <h1></h1>
                        </div>
                        <div className={containers.fullwidth}>
                            <h1></h1>
                        </div>
                        <div className={containers.fullwidth}>
                            <h1></h1>
                        </div>
                    </div>
                    <div className={containers.columned}>
                        <div className={containers.fullwidth}>
                            <h1></h1>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
