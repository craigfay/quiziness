import React from 'react';
import containers from '../styles/containers.module.css';
import text from '../styles/text.module.css';
import { Link } from 'react-router-dom';

export default class SideMenu extends React.Component {

    render() {
        return (
            <div className={containers.sidemenu}>
                <ul>
                    <li><Link to={'/dashboard/example1'}>One</Link></li>
                    <li><Link to={'/dashboard/example2'}>Two</Link></li>
                    <li><Link to={'/login'}>Login</Link></li>
                    <li><Link to={'/register'}>Register</Link></li>
                </ul>
            </div>
        );
    }
}