import React from 'react';
import { Redirect, NavLink } from 'react-router-dom';
import TextField from './TextField';
import api from '../api';
import containers from '../styles/containers.module.css';
import fields from '../styles/fields.module.css';
import text from '../styles/text.module.css';

export default class AccountRegistration extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            submitted: false,
            submissionError: '',
            registered: false,
            messages: []
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.updateValidation = this.updateValidation.bind(this);
        this.formIsComplete = this.formIsComplete.bind(this);
        this.buildRequest = this.buildRequest.bind(this);
    }

    /**
     * Update state when field values change 
     * @param {*} event 
     */
    async handleChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        await this.setState({
          [name]: value
        });
        // Check if invalid fields have been made valid
        if (this.state[`${name}Valid`] === false) {
            this[`${name}Update`]();
        }
    }

    /**
     * Trigger validation errors if the account tries to submit invalid fields
     */
    formIsComplete() {
        const fields = Object.keys(this.state).filter(s => s.includes('IsValid'));
        if (fields.length < this.props.fields.length) {
            // Not all fields have been completed
            this.setState({submissionError: 'Please fill in all fields'})
            return false;
        }
        
        const validity = fields.map(f => this.state[f]);
        if (validity.includes(false)) {
            // There are invalid fields
            this.setState({submissionError: 'Please fix invalid fields'})
            return false;
        }
        
        this.setState({submissionError: ''})
        this.buildRequest();
        return true;
    }

    // Build a request object from the fields prop and their values in state
    buildRequest() {
        const fields = Object.values(this.props.fields).map(f => f.id);
        return fields.reduce((obj, key) => {
            obj[key] = this.state[key]
            return obj;
        }, {})
    }

    renderSubmissionError() {
        return this.state.submissionError;
    }

    /**
     * Validate account input, and send a request to the accounts API 
     * @param {*} event 
     */
    async handleSubmit(event) {
        event.preventDefault();
        if (this.formIsComplete()) {
            const response = await api.register(this.buildRequest());
            // Handle Success 
            if (response.successful) {
                this.setState({registered: true})
            }
            // Handle Failure
            else {
                this.setState({messages: [response.error.message]})
            }
        }
    }

    // Store validation properties in state
    async updateValidation(field, value, validity) {
        await this.setState({
            [field]: value,
            [`${field}IsValid`]: validity
        })
        this.updateMessages()
    }

    renderSuccessMessage() {
        if (this.state.register) {
            return <div className='success'><span>Registration Successful</span></div>
        }
    }

    updateMessages() {
        // Get a list of invalid fields
        const invalidFields = Object.keys(this.state).filter((key) => {
            return key.includes('IsValid') && this.state[key] === false;
        }).map((item) => item.replace('IsValid', ''))

        // Get the requirement messages belonging to each invalid field
        const messages = invalidFields.map(field => this.props.fields[field].requirements)

        this.setState({
            messages: messages
        })
    }

    renderMessages() {
        if (this.state.messages.length < 1) {
            return null;
        }
        return (
            <div className={containers.centerbox}>
                <div className={containers.inner}>
                    <div className={fields.requirements}>{this.state.messages[0]}</div>
                </div>
            </div>
        )
    }

    render() {
        if(this.state.registered) {
            return (
                <Redirect to='/' />
            )
        }
        return (
            <div id="AccountRegistration" className={containers.absolute}>
                <div id="wrap">
                    <div id="header">
                        <div className="messages">{this.renderMessages()}</div>
                    </div>
                    <div id="main-body" className={containers.centerbox}>
                        <div className="inner">
                            <div className={containers.title}>Register</div>
                            <hr />
                            {this.renderSuccessMessage()}
                            <div className={containers.inner}>
                                <form onSubmit={this.handleSubmit}>
                                    {/* Fields  */}
                                    {Object.values(this.props.fields).map((field, i) => 
                                        <TextField
                                            password={field.password}
                                            key={i}
                                            id={field.id}
                                            name={field.name}
                                            pattern={field.pattern}
                                            requirements={field.requirements}
                                            onChange={this.updateValidation}>
                                        </TextField>
                                    )}
                                    {/* Submit */}
                                    <div className={fields.field}>
                                        <button type="submit" className={fields.submit_button}>
                                            <span>Submit</span>
                                        </button>
                                    </div>
                                </form>
                                <hr />
                                <div className={text.centered}>
                                    Already have an account? <NavLink to={'/login'}>Login</NavLink>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
