import React from 'react';
import SideMenu from './SideMenu';
import Example1 from './Example1';
import Example2 from './Example2';
import containers from '../styles/containers.module.css';
import { Route, Switch } from 'react-router-dom';
import { TransitionGroup, CSSTransition } from "react-transition-group";

export default class Dashboard extends React.Component {
    constructor() {
        super();
    }

    selectedView() {
        return (
            <Switch>
                <Route render={({ location }) => (
                    <TransitionGroup className={containers.scrollable}>
                        <CSSTransition
                            key={location.key}
                            classNames="fade"
                            timeout={{enter: 2000, exit: 0}}
                        >
                            <Switch location={location}>
                                <Route path='/dashboard/example1' render={() => <Example1/>}/>
                                <Route path='/dashboard/example2' render={() => <Example2/>}/>
                                <Route render={() => <div>Not Found</div>} />
                            </Switch>
                        </CSSTransition>
                    </TransitionGroup>
                )}>
                ></Route>
            </Switch>
        )
    }
    render() {
        return (
            <div className={containers.columned}>
                    <SideMenu/>
                    {this.selectedView()}
            </div>
        )
    }
}
