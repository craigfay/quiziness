import React from 'react';
import fields from '../styles/fields.module.css';

class TextField extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hasBlurred: false,
            value: '',
            valid: false
        }
        this.onChange = this.onChange.bind(this);
        this.validate = this.validate.bind(this);
        this.onBlur = this.onBlur.bind(this);
    }

    /**
     * Update state when the field value changes
     * @param {*} event 
     */
    onChange(event) {
        this.setState({
          value: event.target.value,
        });
        // Validate the new field value
        return this.validate(event.target.value);
    }

    /**
     * Update state when the field is blurred
     * @todo how can this be made pure?
     */
    async validate(value) {
        if (this.props.pattern.test(value) && this.state.hasBlurred === true) {
            await this.setState({valid: true}) 
            return this.props.onChange(this.props.id, value, true)
        }
        this.setState({valid: false})
        return this.props.onChange(this.props.id, value, this.state.hasBlurred === false); 
    }

    async onBlur(event) {
        await this.setState({hasBlurred: true});
        return this.validate(this.state.value);
    }

    /**
     * Render an error message that describes the desired format of the field
     */
    renderRequirements() {
        if (this.state.valid === false && this.state.hasBlurred === true) {
            return <div className={fields.requirements}>{this.props.requirements}</div>
        } 
        return null
    }
    
    render() {
        return (
            <div className={fields.field + ' ' + ((this.state.valid || this.state.value === '') ? fields.valid : fields.invalid)}>
                {/* {this.renderRequirements()} */}
                <span>
                    <label htmlFor={this.props.id}></label>
                    <input
                        type={this.props.password ? 'password' : 'text'}
                        name={this.props.id}
                        id={this.props.id}
                        placeholder={this.props.name}
                        autoComplete='off'
                        value={this.state.value}
                        onChange={this.onChange}
                        onBlur={this.onBlur}>
                    </input>
                </span>
            </div>
        )
    }
}

export default TextField;