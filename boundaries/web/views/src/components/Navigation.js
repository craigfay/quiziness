import React from 'react';
import { NavLink } from 'react-router-dom';
import styles from '../styles/Navigation.module.css';

export default class Navigation extends React.Component {
    

    renderNavlink(route, label) {
        return <NavLink to={`/${route}`} className={styles.navitem}>{label}</NavLink>
    }
    /**
     * Render the top level login link
     */
    renderLoginLink() {
        if (!this.isLoggedIn()) {
            return this.renderNavlink('login', 'Login');
        }
        return <a className={styles.navitem} href='' onClick={this.logout}>Log Out</a>
    }

    renderRegisterLink() {
        if (!this.isLoggedIn()) {
            return this.renderNavlink('register', 'Register');
        }
    }
    
    renderProtectedLink() {
        if (this.isLoggedIn()) {
            return this.renderNavlink('protected', 'Protected');
        }
    }

    isLoggedIn() {
        return localStorage.getItem('quiziness_token') !== null;
    }

    /**
     * Log the user out of the application
     * @todo Log the user out on all devices
     */
    logout() {
        localStorage.removeItem('quiziness_token')
    }

    render() {
        return (
            <div className={styles.navbar}>
                <NavLink to='/' className={styles.navitem}>Home </NavLink>
                {this.renderLoginLink()}
                {this.renderRegisterLink()}
                {this.renderProtectedLink()}
            </div>
        )
    }
}
