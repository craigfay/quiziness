import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import  api from '../api'

export default class Protected extends Component {
    constructor() {
        super();
        this.state = {
            content: ''
        }
    }

    /**
     * Make a request to the api each time the component mounts`
     */
    async componentWillMount() {

        // Get authorization token
        const token = localStorage.getItem('quiziness_token');

        if (!token) {
            // Navigate to login
            this.setState({content: <Redirect to='/login' />})
            return;
        }

        let response = await api.protected(token);

        if (response.successful === false) {
            this.setState({content: response.error.message})
        }
        else {
            this.setState({content: response.data.message})
        }
    }

    render() {
        return (
            <div>
                <h2>Protected</h2>
                {this.state.content}
            </div>
        )
    }
}
