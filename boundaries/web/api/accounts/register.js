/**
 * Handler for /api/register
 * The purpose of this module is primarily to package the results of...
 * ...UseCase execution into an HTTP response
 * 
 */

/** 
 * Dependencies
 */
const register = require('express').Router();
const AccountRegistration = require('../../../../accounts/usecases/AccountRegistration');
const RequestModel = require('../../../../application/request_model');
const onFailure = require('../../../../application/on_failure');
const ResponseModel = require('../../../../application/response_model');
const ErrorModel = require('../../../../application/error_model');

/**
 * Register a new account
 * @todo should this be returning a response model?
 */
register.post('/', async (req, res, next) => {

    // Package the web request as a RequestModel
    const registerRequest = new RequestModel(req.body);

    // Try to create a new account based on the request
    const execution = await AccountRegistration.execute(registerRequest);
    
    // Return an error if unsuccessful
    if (execution.successful == false) {
        switch (execution.error.type) {
            case 'Invalid Request':
                return res.status(400).json(execution)
            case 'Persistence Error':
                return res.status(500).json(execution)
            default:
                // @todo Log this
                return res.status(500).json(onFailure());
        }
    }

    // Respond affirmatively
    return res.status(200).json(execution)
})

module.exports = register;