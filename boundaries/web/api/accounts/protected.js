/**
 * Handler for /api/register
 * 
 */

/** 
 * Dependencies
 */
const protected = require('express').Router();
const Auth = require('../authorization/Authorization');
// const AccountRegistration = require('../../../../accounts/usecases/AccountRegistration');
// const RequestModel = require('../../../../application/request_model');

/**
 * Register a new account
 * @todo should this be returning a response model?
 */
protected.post('/', Auth.parseToken, (req, res, next) => {
    return res.json({
        successful: true,
        error: null,
        data: {
            message: 'This is the protected content!'
        }
    });

})

module.exports = protected;