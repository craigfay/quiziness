
/**
 * Handler for /api/accounts/login
 * 
 */

/**
 * Dependencies
 */
const login = require('express').Router();
const AccountLogin = require('../../../../accounts/usecases/AccountLogin');
const RequestModel = require('../../../../application/request_model');
const ResponseModel = require('../../../../application/response_model');
const Auth = require('../authorization/Authorization');
const onFailure = require('../../../../application/on_failure');

/**
 * Register a new account
 * @todo should this be returning a response model?
 */
login.post('/', async (req, res, next) => {

    try {

        // Package the web request as a RequestModel
        const loginRequest = new RequestModel(req.body);

        // Run the AccountLogin UseCase
        const execution = await AccountLogin.execute(loginRequest);
        if (execution.successful == false) {
            // If the request was invalid
            if (execution.error.type == 'Invalid Request') {
                return res.status(400).json(execution)
            }
            // Any other reason
            return res.status(500).json(onFailure())
        }

        // Try to create an authorization token for the requester
        const authorization = await Auth.signToken(req.body.email);
        if (authorization.successful == false) {
            return res.status(500).json(authorization)
        }

        // Both execution and authorization were successful
        return res.status(200)
        .json({
            successful: true,
            error: null,
            data: {
                token: authorization.data.token
            }
        })

    }
    catch(e) {
        console.log(e);
        return res.status(500).json({message: 'there was an unexpected server error'});
    }
    
})

module.exports = login;