/**
 * Handle requests to '/api/accounts'
 * 
 */

/**
 * Dependencies
 */
const accounts = require('express').Router();
const register = require('./register');
const login = require('./login');
const protected = require('./protected');

/**
 * /api/accounts/register
 */
accounts.use('/register', register);

/**
 * /api/accounts/login
 */
accounts.use('/login', login);

/**
 * /api/accounts/protected
 */
accounts.use('/protected', protected);

accounts.get('/', (req, res) => {
    res.render('index');
})

module.exports = accounts;