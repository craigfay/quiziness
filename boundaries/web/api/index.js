/**
 * Handle requests to '/api'
 * 
 */

/**
 * Dependencies
 */
const api = require('express').Router();
const accounts = require('./accounts/index');

/**
 * /api/accounts/register
 */
api.use('/accounts', accounts);

api.get('/', (req, res) => {
    res.render('index');
})

module.exports = api;
