/**
 * Tests for Authorization.js
 */

/**
 * Dependencies
 */
const Auth = require('./Authorization');
const ResponseModel = require('../../../../application/response_model');

describe('Authorization Class', () => {
    it('should be a function', () => {
        expect(typeof Auth).toBe('function');
    })

    it('should have a static signToken method', () => {
        expect(Auth).toHaveProperty('signToken');
        expect(typeof Auth.signToken).toBe('function');
    })

    it('should have a static parseToken method', () => {
        expect(Auth).toHaveProperty('parseToken');
        expect(typeof Auth.parseToken).toBe('function');
    })

})

describe('Authorization.signToken', () => {
    it('should return a promise that resolves to a ResponseModel', async () => {
        expect(await Auth.signToken() instanceof ResponseModel).toBe(true);
    })
})

describe('Authorization.parseToken', () => {
    it('should return a promise that resolves to a ResponseModel', async () => {
        expect(await Auth.signToken() instanceof ResponseModel).toBe(true);
    })
})