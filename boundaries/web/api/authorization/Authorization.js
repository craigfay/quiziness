/**
 * Dependencies
 */
const jwt = require('jsonwebtoken');
const ResponseModel = require('../../../../application/response_model');
const ErrorModel = require('../../../../application/error_model');
const { secretkey } = require('../../../../config');

module.exports = class Authorization {

    /**
     * Sign a token that clients can use to prove their identity 
     * @param {String} email 
     * @returns {ResponseModel}
     */
    static async signToken(email) {

        try {
            const token = await jwt.sign({email}, secretkey)
            
            return new ResponseModel(
                true,
                null,
                {token}
            )
        }

        catch(e) {
            return this.onFailure(e);
        }
    }

    /**
     * (Middleware)
     * Parse auth token from request body and add it as a property
     */
    static async parseToken(req, res, next) {
        // Get auth header value
        const bearer = req.body.token;
    
        if (typeof bearer !== 'undefined') {

            // Parse the token header
            const token = bearer.split(' ')[1];

            // Set the token
            req.token = token;
            next();
            
        } else {
            // @todo Use this data in the response
            // @todo Redirect to login
            const response = new ResponseModel(
                false,
                new ErrorModel(
                    'Unauthorized Request',
                    'Missing authorization token'
                )
            ) 
            return res.status(403).json({
                successful: false,
                error: {
                    type: 'Unauthorized Request',
                    message: 'The requested content is only available to authenticated users'
                },
                data: {}
            })
        }
    }

    /**
     * Default error handling for the class
     * @param {Error} e - Any Error Object
     */
    static async onFailure(e) {
        return new ResponseModel(
            false,
            new ErrorModel(
                'Authorization Error',
                'The authorization token was mishandled'
            )
        )
    }

}


