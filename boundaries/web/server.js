/**
 * A Boundary Object that opens communication with the web
 * 
 */

/**
 * Dependencies
 */
const express = require('express');
const api = require('./api/index');
const bodyParser = require('body-parser');
const path = require('path');

/**
 * Export Wrapper
 */
const server = {};

/** @function server.init
 * Start listening for web requests 
 * @param {Object} config - A configuration object with a port property 
 */
server.init = (config) => {
    /**
     * The 'Express App' 
     */
    const app = express();

    /**
     * @todo remove this before deployment
     * Allow Cross Origin Requests
     */
    app.use(function(req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    });

    /**
     * Request Body Parser
     */
    app.use(bodyParser.json())
    app.use(bodyParser.urlencoded({ extended: true })); 


    /**
     * API Router
     */
    app.use('/api', api);

    /**
     * Static Path
     */
    app.use(express.static(path.join(__dirname, 'views', 'build')))

    /**
     * Views
     */
    app.get('/', function(req, res) {
        console.log('Request', req.body);
        res.sendFile(path.join(__dirname, 'views', 'build', 'index.html'));
    });

    /**
     * Start the server
     */
    return app.listen(config.port, () => {
        console.log(`Server listening on port ${config.port} ...`)
    });
}

module.exports = server;
